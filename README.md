# SILENT-LIGHT

## A basic grub2 theme I made for my gentoo box

Font: Inter Regular

Unfortunately, Inter doesn't really play nice with grub-mkfont so I'm looking to replace it. 


To use (may vary with distribution): 
- Clone and enter the repo directory
- Run ```# mv silent-light /boot/grub/themes/```
- Open /etc/default/grub in an escalated text editor and change GRUB_THEME to ```GRUB_THEME="/boot/grub/themes/silent-light/theme.txt"
- Run ```# grub-mkconfig -o /boot/grub/grub.cfg```
- Verify that grub sees your kernel and OS
- reboot and enjoy.

Optionally, you might use [grub2-theme-preview](https://github.com/hartwork/grub2-theme-preview) to verify everything is working OK. 


![Preview image of SILENTLIGHT](/images/preview.png)Screenshot Preview
